package com.mail.demo.controller;

import com.mail.demo.services.EmailSenderService;
import com.mail.demo.util.MessageResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.web.bind.annotation.*;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/api")
public class SendMail {

    @Autowired
    private JavaMailSender javaMailSender;

    @Autowired
    private EmailSenderService emailSenderService;

    @GetMapping("/sendMail")
    public ResponseEntity<?> sendMailOTP(@RequestParam(name = "email") String email, @RequestParam(name = "otp") String otp) {
        try{
        this.emailSenderService.sendNotificationEmail(email,
                "OTP for GenAI Demo Application", "Your OTP is : "+otp);
            return ResponseEntity.ok(new MessageResponse("Mail sent Successfully !"));
        } catch ( Exception e){
            return new ResponseEntity<>(new MessageResponse(" Something went wrong !"), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

}
