package com.mail.demo.services;

import java.util.HashMap;
import java.util.Map;
import java.util.Stack;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
public class EmailSenderService implements Runnable {
    @Autowired
    private JavaMailSender javaMailSender;

    private Stack<SimpleMailMessage> stack = new Stack();

    public EmailSenderService() {
    }

    public Thread sendEmail(String toEmail, String subject, String body) {
        SimpleMailMessage message = new SimpleMailMessage();
        message.setFrom("CTHospitalO@gmail.com");
        message.setTo(toEmail);
        message.setSubject(subject);
        message.setText(body);
        this.stack.push(message);
        Thread emailSenderThread = new Thread(this);
        emailSenderThread.start();
        return emailSenderThread;
    }

    public void run() {
        SimpleMailMessage message = (SimpleMailMessage)this.stack.pop();
        try {
            this.javaMailSender.send(message);
        } catch (Exception var3) {
            var3.printStackTrace();
        }
    }

    public boolean sendNotificationEmail(String sendTo, String subject, String body) {
        this.sendEmail(sendTo, subject, body);
        return false;
    }
}
