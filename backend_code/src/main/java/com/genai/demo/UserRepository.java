package com.genai.demo;

import java.util.Optional;

import com.genai.demo.payload.Story;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.genai.demo.payload.User;

@Repository
public interface UserRepository extends JpaRepository<User, Integer> {
	Optional<User> findByUsername(String username);

	Boolean existsByUsername(String username);

	Boolean existsByEmail(String email);

	Optional<User> findByEmail(String email);

	@Query(value = "SELECT * FROM user WHERE user_id = ?1", nativeQuery = true)
	Optional<User> findByUserId(int parseInt);

}
