package com.genai.demo.util;

import com.genai.demo.payload.Story;
import com.genai.demo.payload.StoryDTO;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public interface UserService {

	public List<StoryDTO> getStory(Integer userId);

	public List<StoryDTO> getAllStories();

}