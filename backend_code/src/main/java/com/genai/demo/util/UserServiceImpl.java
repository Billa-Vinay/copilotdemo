package com.genai.demo.util;

import com.genai.demo.StoryRepository;
import com.genai.demo.UserRepository;
import com.genai.demo.payload.Story;
import com.genai.demo.payload.StoryDTO;
import com.genai.demo.payload.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class UserServiceImpl implements UserService{

    @Autowired
    private StoryRepository storyRepository;

    @Autowired
    private UserRepository userRepository;

    @Override
    public List<StoryDTO> getStory(Integer userId) {
        User u = userRepository.findByUserId((userId)).get();
        System.out.println(u);
        List<Story> allStories = storyRepository.findByUserId(userId);
        System.out.println(allStories);
        if(allStories.isEmpty()){
            return new ArrayList<>();
        }
        List<StoryDTO> result = new ArrayList<StoryDTO>();
        for(Story s : allStories){
            System.out.println(s);
            StoryDTO sd = new StoryDTO();
            sd.setId(s.getId());
            sd.setStoryName(s.getStoryName());
            sd.setStoryRemarks(s.getStoryRemarks());
            sd.setStoryDescription(s.getStoryDescription());
            sd.setUser(u.getId());
            sd.setUserName(u.getUsername());
            sd.setUpdatedDate(s.getUpdatedDate());
            result.add(sd);
            System.out.println(sd);
        }
        System.out.println(result);
        return result.stream().sorted(Comparator.comparing(StoryDTO::getUpdatedDate).reversed())
                .collect(Collectors.toList());
    }

    @Override
    public List<StoryDTO> getAllStories() {
        List<Story> allStories = storyRepository.findAll();
        List<User> allUsers = userRepository.findAll();
        List<StoryDTO> result = new ArrayList<StoryDTO>();
        for(User u : allUsers){
            result.addAll(getStory(u.getId()));
        }
        return result.stream().sorted(Comparator.comparing(StoryDTO::getUpdatedDate).reversed())
                .collect(Collectors.toList());
    }
}
