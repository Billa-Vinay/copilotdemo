package com.genai.demo;

import java.util.Optional;
import java.util.Properties;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import com.genai.demo.util.UserService;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;

import com.genai.demo.payload.Story;
import com.genai.demo.payload.User;
import com.genai.demo.payload.request.LoginRequest;
import com.genai.demo.payload.request.SignupRequest;
import com.genai.demo.payload.request.StoryRequest;
import com.genai.demo.payload.response.MessageResponse;
import com.genai.demo.payload.response.UserInfoResponse;

import jakarta.validation.Valid;

//for Angular Client (withCredentials)
@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/api")
public class UserController {

	Logger logger = org.slf4j.LoggerFactory.getLogger(UserController.class);

	@Autowired
	private UserService userService;

	@Autowired
	UserRepository userRepository;

	@Autowired
	StoryRepository storyRepository;

	@Autowired
	PasswordEncoder encoder;

	@PostMapping("/signin")
	public ResponseEntity<?> authenticateUser(@Valid @RequestBody LoginRequest loginRequest) {

		Optional<User> optUser = userRepository.findByEmail(loginRequest.getEmail());

		if (!optUser.isPresent()) {
			return ResponseEntity.badRequest().body("User not found");
		}
		User user = optUser.get();
		if (!encoder.matches(loginRequest.getPassword(), user.getPassword())) {
			return ResponseEntity.badRequest().body("Password is incorrect");
		}

		return ResponseEntity.ok().body(new UserInfoResponse(user.getId(),
				user.getUsername(),
				user.getEmail(),
				user.getRole()));
	}

	@PostMapping("/signup")
	public ResponseEntity<?> registerUser(@Valid @RequestBody SignupRequest signUpRequest) {
		if (userRepository.existsByEmail(signUpRequest.getEmail())) {
			return ResponseEntity.badRequest().body(new MessageResponse("Error: Email is already in use!"));
		}

		// Create new user's account
		User user = User.builder().username(signUpRequest.getUsername())
				.email(signUpRequest.getEmail())
				.password(encoder.encode(signUpRequest.getPassword()))
				.phone(signUpRequest.getPhone())
				.build();

		String strRoles = signUpRequest.getRole();

		if (signUpRequest.getRole() == null || signUpRequest.getRole().equals("")) {
			strRoles = "ROLE_USER";
		} else if (signUpRequest.getRole().equals("admin")) {
			strRoles = "ROLE_ADMIN";
		} else if (signUpRequest.getRole().equals("lead")) {
			strRoles = "ROLE_LEAD";
		} else {
			strRoles = "ROLE_USER";
		}

		user.setRole(strRoles);
		userRepository.save(user);

		return ResponseEntity.ok(new MessageResponse("User registered successfully!"));
	}

	@GetMapping("/forgotPassword")
	public ResponseEntity<?> forgotPass(@RequestParam(name = "email") String email) {
		Boolean userExists = userRepository.existsByEmail(email);
		if (!userExists) {
			return new ResponseEntity<>(
					"Email is invalid",
					HttpStatus.BAD_REQUEST);
		}
		return new ResponseEntity<>(
				"Email is valid",
				HttpStatus.OK);
	}

	@GetMapping("/getUserByMail")
	public ResponseEntity<?> getUserByEmail(@RequestParam(name = "email") String email) {
		Boolean userExists = userRepository.existsByEmail(email);
		if (!userExists) {
			return new ResponseEntity<>(
					"No user has this email",
					HttpStatus.BAD_REQUEST);
		}
		return new ResponseEntity<>(userRepository.findByEmail(email), HttpStatus.OK);
	}

	@PutMapping("/resetPassword")
	public ResponseEntity<?> resetPass(@RequestBody User user) {
		try {
			Optional<User> oldUser = userRepository.findByEmail(user.getEmail());
			if (oldUser.isPresent()) {
				oldUser.get().setPassword(encoder.encode(user.getPassword()));
			}
			userRepository.save(oldUser.get());
			return new ResponseEntity<>(new MessageResponse("Password reset successfully"), HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(new MessageResponse(" Something went wrong. Try again !"),
					HttpStatus.BAD_REQUEST);
		}
	}

	@PostMapping("/uploadStory")
	public ResponseEntity<?> uploadStory(@RequestBody StoryRequest request) {
		Story story = Story.builder()
				.storyName(request.getStoryName())
				.storyDescription(request.getDescription())
				.storyRemarks(request.getRemarks())
				.user(request.getUserId())
				.updatedDate(new java.util.Date())
				.build();
		storyRepository.save(story);
		return ResponseEntity.ok(new MessageResponse("Story uploaded successfully!"));
	}

	@PutMapping("/updateStory")
	public ResponseEntity<?> updateStory(@Valid @RequestBody Story story) {
		try {
			story.setUpdatedDate(new java.util.Date());
			storyRepository.save(story);
			return ResponseEntity.ok(new MessageResponse("Story Updated !"));
		} catch (Exception e) {
			return new ResponseEntity<>(" Something went wrong. Try again !", HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@GetMapping("/getUser")
	public ResponseEntity<?> getUser(
			@RequestParam(value = "userId", required = false, defaultValue = "") String userId) {
		if (userId == null || userId.equals("")) {
			return ResponseEntity.ok(userRepository.findAll());
		}
		return ResponseEntity.ok(userRepository.findById(Integer.parseInt(userId)));
	}

	@GetMapping("/getStories")
	public ResponseEntity<?> getStories(@RequestParam(value = "userId") String userId) {
		if (userId.equals("null")) {
			return ResponseEntity.ok(userService.getAllStories());
		} else {
			return ResponseEntity.ok(userService.getStory(Integer.parseInt(userId)));
		}
	}

	@PutMapping("/updateUser")
	public ResponseEntity<?> updateUser(@Valid @RequestBody User user) {
		try {
			userRepository.save(user);
			return ResponseEntity.ok(new MessageResponse("Role change to " + user.getRole()));
		} catch (Exception e) {
			return new ResponseEntity<>(" Something went wrong. Try again !", HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@DeleteMapping("deleteStory")
	public ResponseEntity<?> deleteStory(@RequestParam(value = "storyId") Integer id) {
		try {
			storyRepository.deleteById(id);
			return ResponseEntity.ok(new MessageResponse("Story Deleted Successfully !"));
		} catch (Exception e) {
			return new ResponseEntity<>(" Something went wrong. Try again !", HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

}
