package com.genai.demo.payload;

import java.util.Date;

import org.springframework.format.annotation.DateTimeFormat;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import jakarta.validation.constraints.NotBlank;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Table(name = "stories")
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Story {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "story_id")
	private Integer id;
	
	@Column(name = "story_name")
	@NotBlank
	private String storyName;
	
	@Column(name = "description")
	private String storyDescription;
	
	@Column(name = "remarks")
	private String storyRemarks;
	
	@Column(name = "user_id")
	private Integer user;
	
	@Column(name = "last_modified")
//	@DateTimeFormat(pattern = "dd-MM-yyyy")
	private Date updatedDate;
	
}
