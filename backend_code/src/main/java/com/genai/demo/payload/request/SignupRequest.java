package com.genai.demo.payload.request;

import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.Size;
import lombok.Data;
 
@Data
public class SignupRequest {
    @NotBlank
    private String username;
 
    @NotBlank
    @Email
    private String email;
    
    private String role;
    
    @NotBlank
    private String password;
    
    private String phone;
  
}
