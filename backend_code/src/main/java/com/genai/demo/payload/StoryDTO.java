package com.genai.demo.payload;

import jakarta.persistence.*;
import jakarta.validation.constraints.NotBlank;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class StoryDTO {
    private Integer id;
    private String storyName;
    private String storyDescription;
    private String storyRemarks;
    private Integer user;
    private Date updatedDate;
    private String userName;

}
