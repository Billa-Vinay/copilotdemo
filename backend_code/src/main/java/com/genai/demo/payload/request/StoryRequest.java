package com.genai.demo.payload.request;

import jakarta.validation.constraints.NotBlank;
import lombok.Data;

@Data
public class StoryRequest {
    @NotBlank
    private String storyName;
 
    @NotBlank
    private String description;
    
    private String remarks;
    
    @NotBlank
    private Integer userId;    
  
}

