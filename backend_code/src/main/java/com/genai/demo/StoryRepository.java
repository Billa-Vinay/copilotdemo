package com.genai.demo;

import java.util.List;
import java.util.Optional;

import com.genai.demo.payload.StoryDTO;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.genai.demo.payload.Story;

@Repository
public interface StoryRepository extends JpaRepository<Story, Integer> {
	
	@Query(value = "SELECT * FROM stories WHERE user_id = ?1", nativeQuery = true)
	List<Story> findByUserId(int parseInt);

}