import { HttpClient } from '@angular/common/http';
import { Component, Input } from '@angular/core';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ApiServiceService } from '../api-service.service';
import { TYPE } from '../constants';

@Component({
  selector: 'app-verification',
  templateUrl: './verification.component.html',
  styleUrls: ['./verification.component.css']
})
export class VerificationComponent {

  @Input() email: string;
  verificationForm: FormGroup;
  otp: number;

  constructor(http: HttpClient, private formBuilder: FormBuilder, private router: Router, private service: ApiServiceService) {
    this.email = router.getCurrentNavigation()?.extras.state['email'];
    this.generateOTP();

    this.service.sendEmail(this.email, this.otp).subscribe((res) => 
      { this.service.toast(TYPE.SUCCESS, true, 'OTP sent to your email id') },
      (error) => { this.service.toast(TYPE.ERROR, true, 'Error while sending OTP') }
    );
  }

  ngOnInit() {
    this.verificationForm = this.formBuilder.group({
      otp: new FormControl('', Validators.required),
    });
  }

  onSubmit(){
    if(this.verificationForm.invalid){
      this.verificationForm.markAllAsTouched();
      this.service.toast(TYPE.WARNING, true, 'Please enter the OTP');
      return;
    }
    const otp = this.verificationForm.controls['otp']?.value;
    if(otp !== this.otp){
      this.service.toast(TYPE.ERROR, true, 'Invalid OTP Entered'); 
      return;
    }
    if(otp === this.otp){
      this.service.toast( TYPE.SUCCESS, true, 'Account verified successfully')
      this.router.navigateByUrl('/dashboard');
    }
  }

  generateOTP(){
    this.otp = Math.round(Math.random() * (999999 - 100000 +1) + 100000);
    console.log(this.otp);
  }
  
}
