import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import Swal from 'sweetalert2';
import { TYPE } from './constants';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ApiServiceService {
  
  addStory(value: any) : Observable<any> {
    return this.http.post("http://localhost:8082/api/uploadStory", value);  
  }
  editStory(value: any) : Observable<any> {
    return this.http.put("http://localhost:8082/api/updateStory", value);  
  }
  updateStory(id: any, value: any) {
    return this.http.put("https://localhost:44380/api/Story/UpdateStory/" + id, value);
  }
  deleteStory(id: any): Observable<any> {
    return this.http.delete("http://localhost:8082/api/deleteStory?storyId="+id);
  }
  getAllStories(): Observable<any>{
    return this.http.get("http://localhost:8082/api/getStories")
  }
  getStories(userId): Observable<any>{
    return this.http.get("http://localhost:8082/api/getStories?userId="+userId);
  }
  getAllUsers(): Observable<any> {
    return this.http.get('http://localhost:8082/api/getUser');
  }
  changeRole(user): Observable<any>{
    return this.http.put('http://localhost:8082/api/updateUser' ,user);
  }
  constructor(private http: HttpClient) { }

  getUserByMail(email): Observable<any> {
    return this.http.get('http://localhost:8082/api/getUserByMail?email='+email);
  }
  sendEmail(email: any, otp: any) {
    return this.http.get("http://localhost:8084/api/sendMail?email="+email+"&otp="+otp);
  }
  login(params: any) : Observable<any>{
    return this.http.post('http://localhost:8082/api/signin', params);
  }
  register(params: any): Observable<any>{
    return this.http.post('http://localhost:8082/api/signup', params);
  }
  isLoggedIn() {
    return localStorage.getItem('userObject') === null ? false : true;
  }
  resetPassword(user:any): Observable<any>{
    return this.http.put('http://localhost:8082/api/resetPassword' ,user);
  }
  toast(typeIcon: TYPE, timerProgressBar: boolean, message: string) {
    Swal.fire({
      toast: true,
      position: 'top',
      showConfirmButton: false,
      icon: typeIcon,
      timerProgressBar: timerProgressBar,
      timer: 3000,
      title: message
    })
  }
  
}
