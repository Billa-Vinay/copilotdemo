import { HttpClient } from '@angular/common/http';
import { MatDialog } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { AddEditStoryComponent } from '../add-edit-story/add-edit-story.component';
import { PeriodicElement } from '../modal/PeriodicElement';
import { ApiServiceService } from '../api-service.service';
import { TYPE } from '../constants';
import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css']
})
export class UsersComponent implements OnInit, AfterViewInit{

  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) sort: MatSort;
  displayedColumns: string[] = ['role', 'username', 'phone', 'email'];
  dataSource = new MatTableDataSource<PeriodicElement>();
  rowData : any = [];
  isAdmin: boolean = false;
  userId: any;

  constructor(private http: HttpClient, private dialog: MatDialog, private service: ApiServiceService) { 
  }

  ngOnInit(): void {
    let retrievedObject = localStorage.getItem('userObject');
    this.isAdmin = JSON.parse(retrievedObject)?.role === 'ROLE_ADMIN';
    this.userId = JSON.parse(retrievedObject)?.id;
    
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
    this.loadUsers();
  }

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  loadUsers() {
    this.service.getAllUsers().subscribe((res:any)=>{
      this.rowData = res;
      this.dataSource = new MatTableDataSource<any>(this.rowData);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
    })
  }

  openDialog(data: any) {
    this.dialog.open(AddEditStoryComponent, { data: data, width: '500px', disableClose: true }).afterClosed().subscribe(res => {
      if (res) {
        this.loadUsers();
      }
     });
  }

  applyFilter(event: any) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  } 
  
  roleChange(params: any ){
    if(params.id === this.userId){
      this.service.toast(TYPE.WARNING, true, 'You cannot change your own role');
      this.loadUsers();
      return;
    } 
    params.role = params.role === 'ROLE_ADMIN' ? 'ROLE_USER' : 'ROLE_ADMIN';
    this.service.changeRole(params).subscribe(res => {
      this.loadUsers();
      this.service.toast(TYPE.SUCCESS, true, 'Role Changed Successfully !');
    }, error => {
      this.service.toast(TYPE.ERROR, true, error?.error)
    })
  }

}