import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ApiServiceService } from 'src/app/api-service.service';
import { TYPE } from 'src/app/constants';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  loginForm: FormGroup;
  registerForm: FormGroup;

  constructor(private http: HttpClient, private service: ApiServiceService ,private router: Router, private formBuilder: FormBuilder) {}

  ngOnInit() {
    localStorage.clear();
    this.loginForm = this.formBuilder.group({
      email: new FormControl('', [Validators.required, Validators.email]),
      password: new FormControl('', Validators.required)
    });

    this.registerForm= this.formBuilder.group({
      userName: new FormControl('', Validators.required),
      userEmail: new FormControl('', Validators.required),
      userPassword: new FormControl('', Validators.required),
      number: new FormControl('', Validators.required ),
      confirmPassword: new FormControl('', Validators.required)
    });
  }

  onLogin() {
    if(this.loginForm.controls['email'].invalid){
      this.registerForm.markAllAsTouched();
      this.service.toast(TYPE.INFO, true, 'Enter a valid email');
      return;
    }
    if(this.loginForm.invalid){
      this.loginForm.markAllAsTouched();
      this.service.toast(TYPE.INFO, true, 'Please fill all the fields');
      return;
    }
    const email = this.loginForm.get('email')?.value;
    const password = this.loginForm.get('password')?.value;
    let loginObj = {
      email: email,
      password: password
    };
    
    this.service.login(loginObj).subscribe((res: any) => {
      if (res) {      
        this.service.toast(TYPE.SUCCESS, true, 'Signed In Successfully');
        localStorage.setItem('userObject', JSON.stringify(res));
        this.router.navigateByUrl('/dashboard');
      }
     }, error => {
        this.service.toast(TYPE.ERROR, true, error?.error);
     });
  }

  onRegister() {
    let validRegex = /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/;
    if(this.registerForm.invalid){
      this.registerForm.markAllAsTouched();
      this.service.toast(TYPE.INFO, true, 'Please fill all the fields');
      return;
    }
    if(String(this.registerForm.controls['number'].value).length !== 10){
      this.registerForm.markAllAsTouched();
      this.service.toast(TYPE.INFO, true, 'Enter a valid 10 digit number');
      return;
    }
    if(!this.registerForm.controls['userEmail'].value.match(validRegex)){
      this.registerForm.markAllAsTouched();
      this.service.toast(TYPE.INFO, true, 'Enter a valid email');
      return;
    }
    if(String(this.registerForm.get('userPassword')?.value).length < 8){
      this.registerForm.markAllAsTouched();
      this.service.toast(TYPE.INFO, true, 'The password must be at least 8 characters long');
      return;
    }
    if(this.registerForm.get('userPassword')?.value !== this.registerForm.get('confirmPassword')?.value){ 
      this.registerForm.markAllAsTouched();
      this.service.toast(TYPE.INFO, true, 'Passwords do not match');
      return;
    }
    const userName = this.registerForm.get('userName')?.value;
    const userEmail = this.registerForm.get('userEmail')?.value;
    const userPassword = this.registerForm.get('userPassword')?.value;
    const contact = this.registerForm.get('number')?.value;
    let registerObj = {
      username: userName,
      phone: contact,
      email: userEmail,
      password: userPassword,
      role: null
    };
    
    this.service.register(registerObj).subscribe((res: any) => {
      if (res) {
        this.service.toast(TYPE.SUCCESS, true, res.message);
        this.router.navigate(['verification'], { state: { email: userEmail } });
      }
     }, error => {
        this.service.toast(TYPE.ERROR, true, error?.message);
    });
  }

}


