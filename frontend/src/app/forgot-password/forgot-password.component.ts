import { HttpClient } from '@angular/common/http';
import { Component } from '@angular/core';
import { Form, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import Swal from 'sweetalert2';
import { TYPE } from '../constants';
import { Router } from '@angular/router';
import { ApiServiceService } from '../api-service.service';

@Component({
  selector: 'app-forgot-password',
  templateUrl: './forgot-password.component.html',
  styleUrls: ['./forgot-password.component.css']
})
export class ForgotPasswordComponent {

  forgotForm: FormGroup;
  displayOtp: boolean= false;
  otp: number= 0;
  displayPassword: boolean= false;
  
  constructor(http: HttpClient, private formBuilder: FormBuilder, private router: Router, private service: ApiServiceService) {}

  ngOnInit() {
    this.forgotForm = this.formBuilder.group({
      email: new FormControl('', [Validators.required, Validators.email]),
      otp: new FormControl('', Validators.required),
      password: new FormControl('', Validators.required),
      confirmPassword: new FormControl('', Validators.required)
    });
  }

  getOTP() {
    if (this.forgotForm.controls['email'].invalid) {
      this.forgotForm.markAllAsTouched();
      this.toast(TYPE.INFO, true, 'Please enter a valid email');
      return;
    }
    const email = this.forgotForm.controls['email'].value;
    this.service.getUserByMail(email).subscribe( res => {
      this.generateOTP();
      this.service.sendEmail(email, this.otp).subscribe(res => {
        this.toast(TYPE.SUCCESS, true, 'OTP sent to your email');
      }, error => { 
        this.toast(TYPE.ERROR, true, 'Something went wrong. Try again !');
      });
      this.displayOtp = true;
    }, error=> {
      this.toast(TYPE.ERROR, true, error?.error);
    });
  }

  confirmOTP() {
    if (this.forgotForm.controls['otp'].invalid) {
      this.forgotForm.markAllAsTouched();
      this.toast(TYPE.INFO, true, 'Please enter the OTP');
      return;
    }
    const otp = this.forgotForm.get('otp')?.value;
    otp === this.otp ? this.displayPassword = true : this.toast(TYPE.ERROR, true, 'Incorrect OTP');
    console.log(otp);
  }

  onResetPassword() {
    if (this.forgotForm.controls['password'].invalid) {
      this.forgotForm.markAllAsTouched();
      this.toast(TYPE.INFO, true, 'Please enter a valid password');
      return;
    }
    if (this.forgotForm.controls['confirmPassword'].invalid) {
      this.forgotForm.markAllAsTouched();
      this.toast(TYPE.INFO, true, 'Please enter a valid password');
      return;
    }
    const password = this.forgotForm.get('password')?.value;
    const confirmPassword = this.forgotForm.get('confirmPassword')?.value;
    if (password !== confirmPassword) {
      this.toast(TYPE.ERROR, true, 'Passwords do not match');
      return;
    }
    const email = this.forgotForm.controls['email'].value;
    this.service.resetPassword({email: email, password: password}).subscribe(res => {
      this.toast(TYPE.SUCCESS, true, 'Password reset successfully');
      this.router.navigateByUrl('/login');
    }, error => {
      this.toast(TYPE.ERROR, true, 'Something went wrong. Try again !');
    });
  }

  generateOTP(){
    this.otp = Math.round(Math.random() * (999999 - 100000 +1) + 100000);
    console.log(this.otp);
  }

  toast(typeIcon: TYPE, timerProgressBar: boolean, message: string) {
    Swal.fire({
      toast: true,
      position: 'top',
      showConfirmButton: false,
      icon: typeIcon,
      timerProgressBar: timerProgressBar,
      timer: 3000,
      title: message
    })
  }
  
}
