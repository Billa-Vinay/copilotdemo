import { Component, OnInit } from '@angular/core';
import { ApiServiceService } from '../api-service.service';
import { Router } from '@angular/router';
import { TYPE } from '../constants';

@Component({
  selector: 'app-header-bar',
  templateUrl: './header-bar.component.html',
  styleUrls: ['./header-bar.component.css']
})
export class HeaderBarComponent implements OnInit {

  isLoggedIn = false;
  username : string = '' ;

  constructor(private service: ApiServiceService,private router: Router) { }

  ngOnInit(): void {
    this.isLoggedIn = this.service.isLoggedIn();
    if(this.isLoggedIn){
      let retrievedObject = localStorage.getItem('userObject');
      this.username = JSON.parse(retrievedObject)?.username;
    }
  }

  logout(){
    localStorage.removeItem('userObject');
    this.service.toast(TYPE.SUCCESS, true, 'Logged out successfully');
    this.isLoggedIn = false;
    this.router.navigate(['/login']);
  }

}
