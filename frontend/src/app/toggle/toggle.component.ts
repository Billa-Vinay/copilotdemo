import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-toggle',
  templateUrl: './toggle.component.html',
  styleUrls: ['./toggle.component.css']
})
export class ToggleComponent implements OnInit{

  isAdmin: boolean = false;

  ngOnInit(): void {
    let retrievedObject = localStorage.getItem('userObject');
    this.isAdmin = JSON.parse(retrievedObject)?.role === 'ROLE_ADMIN';

    this.selectedVal = 'stories';
  }

  toggle: string;
  selectedVal: string;

  onValChange(val: string) {
    this.selectedVal = val;
  }

}
