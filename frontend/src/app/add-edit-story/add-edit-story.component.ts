import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { ApiServiceService } from '../api-service.service';
import { TYPE } from '../constants';

@Component({
  selector: 'app-add-edit-story',
  templateUrl: './add-edit-story.component.html',
  styleUrls: ['./add-edit-story.component.css']
})
export class AddEditStoryComponent implements OnInit {
  storyForm: FormGroup<any>;
  action: string;
  isAdmin: boolean = false;
  userId : any;

  constructor(
    private formBuilder: FormBuilder, public service: ApiServiceService,
    public dialogRef: MatDialogRef<AddEditStoryComponent>,
    @Inject(MAT_DIALOG_DATA) public dialogData: any
  ) { }
  
  ngOnInit(): void {
    let retrievedObject = localStorage.getItem('userObject');
    this.isAdmin = JSON.parse(retrievedObject)?.role === 'ROLE_ADMIN';
    this.userId = JSON.parse(retrievedObject)?.id;

    this.storyForm = this.formBuilder.group({ 
      storyNumber: new FormControl('', Validators.required),
      description: new FormControl('', Validators.required),
      remarks: new FormControl(''),
    });
    this.action = this.dialogData === null ? 'Add' : 'Edit';
    if(this.action === 'Edit'){
      this.setValues(this.dialogData);
    }
  }

  setValues(data: any){   
    this.storyForm.patchValue({
      storyNumber: data.storyName,
      description: data.storyDescription,
      remarks: data.storyRemarks === null || data.storyRemarks === '' ? '' : data.storyRemarks
    });
  }

  onSubmit() {
    if (this.storyForm.invalid) {
      this.storyForm.markAllAsTouched();
      this.service.toast(TYPE.ERROR, true, 'Please fill all the required fields');
      return;
    }  
    if(this.action === 'Add'){
      let params: any =  {
      "storyName" : this.storyForm.controls['storyNumber'].value,
      "description" : this.storyForm.controls['description'].value,
      "remarks" : this.storyForm.controls['remarks'].value === '' ? null : this.storyForm.controls['remarks'].value,
      "userId" : this.userId
    }
      this.service.addStory(params).subscribe((res: any) => {
        this.service.toast(TYPE.SUCCESS, true, 'Story added successfully');
        this.dialogRef.close(true);
      }, (err: any) => {
        this.service.toast(TYPE.ERROR, true, 'Something went wrong');
      });
    }else{
      let params: any =  {
        "id" : this.dialogData.id,
        "storyName" : this.storyForm.controls['storyNumber'].value,
        "storyDescription" : this.storyForm.controls['description'].value,
        "storyRemarks" : this.storyForm.controls['remarks'].value === '' ? null : this.storyForm.controls['remarks'].value,
        "user" : this.dialogData.user,
        "updatedDate": new Date()
      }
      this.service.editStory(params).subscribe((res: any) => {
        this.service.toast(TYPE.SUCCESS, true, 'Story updated successfully');
        this.dialogRef.close(true);
      }, (err: any) => {
        this.service.toast(TYPE.ERROR, true, 'Something went wrong');
      });
    }
  }

  onCancel() {
    this.dialogRef.close();
  }

  onClear() {
    this.storyForm.reset();
  }

}
