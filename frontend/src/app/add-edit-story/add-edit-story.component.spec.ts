import { ComponentFixture, TestBed } from '@angular/core/testing';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RouterModule } from '@angular/router';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ApiServiceService } from '../api-service.service';
import { AddEditStoryComponent } from './add-edit-story.component';
import { BrowserDynamicTestingModule } from '@angular/platform-browser-dynamic/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { MatFormField, MatFormFieldModule, matFormFieldAnimations } from '@angular/material/form-field';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';
import {MatDialogModule} from '@angular/material/dialog';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/compiler';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { TYPE } from '../constants';
import { throwError } from 'rxjs';
import { of } from 'rxjs';

const dialogData = {
  "storyId": 1,
  "storyName": "Story 1",
  "storyDescription": "Story 1 Description",
  "storyRemarks": null,
  "userId": 1
}
const matDialogRef = jasmine.createSpy('close');

fdescribe('AddEditStoryComponent', () => {
  let component: AddEditStoryComponent;
  let fixture: ComponentFixture<AddEditStoryComponent>;

  beforeEach(async () => {
    TestBed.configureTestingModule({
      declarations: [AddEditStoryComponent],
      imports: [
        BrowserAnimationsModule, MatDialogModule, ReactiveFormsModule,
        RouterModule, HttpClientTestingModule, ReactiveFormsModule,
        BrowserDynamicTestingModule
      ],      providers: [
        ApiServiceService,
        { provide: MatDialogRef, useValue: matDialogRef },
        { provide: MAT_DIALOG_DATA, useValue: dialogData }
      ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AddEditStoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should initialize the component', () => {
    expect(component).toBeTruthy();
  });
  it('should initialize the component', () => {
    // Arrange
    const retrievedObject = JSON.stringify({
      role: 'ROLE_ADMIN',
      id: 1
    });
    spyOn(localStorage, 'getItem').and.returnValue(retrievedObject);

    // Act
    component.ngOnInit();

    // Assert
    expect(component.isAdmin).toBe(true);
    expect(component.userId).toBe(1);
    expect(component.storyForm).toBeDefined();
  });

  it('should set action to Edit and call setValues when dialogData is not null', () => {
    // Arrange
    const retrievedObject = JSON.stringify({
      role: 'ROLE_ADMIN',
      id: 1
    });
    spyOn(localStorage, 'getItem').and.returnValue(retrievedObject);
    component.dialogData = {
      storyId: 1,
      storyName: 'Story 1',
      storyDescription: 'Story 1 Description',
      storyRemarks: null,
      userId: 1
    };
    spyOn(component, 'setValues');

    // Act
    component.ngOnInit();

    // Assert
    expect(component.action).toBe('Edit');
    expect(component.setValues).toHaveBeenCalledWith(component.dialogData);
  });

  it('should set values in the form', () => {
    // Arrange
    const data = {
      storyName: 'Story 1',
      storyDescription:  'Story 1 Description',
      storyRemarks: null
    };
  
    // Act
    component.setValues(data);
  
    // Assert
    expect(component.storyForm.value.storyNumber).toBe('Story 1');
    expect(component.storyForm.value.description).toBe('Story 1 Description');
    expect(component.storyForm.value.remarks).toBe('');
  });
  it('should call toast with error message and return if form is invalid', () => {
    // Arrange
    spyOn(component.storyForm, 'invalid' as any).and.returnValue(true);
    spyOn(component.storyForm, 'markAllAsTouched');
    spyOn(component.service, 'toast');
  
    // Act
    component.onSubmit();
  
    // Assert
    expect(component.storyForm.markAllAsTouched).toHaveBeenCalled();
    expect(component.service.toast).toHaveBeenCalledWith(TYPE.ERROR, true, 'Please fill all the required fields');
  });
  
  it('should call addStory and close the dialog if action is Add', () => {
    // Arrange
    spyOn(component.storyForm, 'invalid' as any).and.returnValue(false);
    component.action = 'Add';
    component.storyForm.controls['storyNumber'].setValue('Story 1');
    component.storyForm.controls['description'].setValue('Story 1 Description');
    component.storyForm.controls['remarks'].setValue('');
    spyOn(component.service, 'addStory').and.returnValue(of({}));
    spyOn(component.service, 'toast');
    spyOn(component.dialogRef, 'close');
  
    // Act
    component.onSubmit();
  
    // Assert
    expect(component.service.addStory).toHaveBeenCalledWith({
      "storyName": "Story 1",
      "description": "Story 1 Description",
      "remarks": null,
      "userId": component.userId
    });
    expect(component.service.toast).toHaveBeenCalledWith(TYPE.SUCCESS, true, 'Story added successfully');
    expect(component.dialogRef.close).toHaveBeenCalledWith(true);
  });
  
  it('should call editStory and close the dialog if action is not Add', () => {
    // Arrange
    spyOn(component.storyForm, 'invalid' as any).and.returnValue(false);
    component.action = 'Edit';
    component.dialogData = {
      id: 1,
      user: 'user'
    };

    component.storyForm.controls['storyNumber'].setValue('Story 1');
    component.storyForm.controls['description'].setValue('Story 1 Description');
    component.storyForm.controls['remarks'].setValue('');
    spyOn(component.service, 'editStory').and.returnValue(of({}));
    spyOn(component.service, 'toast');
    spyOn(component.dialogRef, 'close');

    // Act
    component.onSubmit();
  
    // Assert
    expect(component.service.editStory).toHaveBeenCalledWith({
      "id": 1,
      "storyName": "Story 1",
      "storyDescription": "Story 1 Description",
      "storyRemarks": null,
      "user": "user",
      "updatedDate": jasmine.any(Date)
    });
    expect(component.service.toast).toHaveBeenCalledWith(TYPE.SUCCESS, true, 'Story updated successfully');
    expect(component.dialogRef.close).toHaveBeenCalledWith(true);
  });
  
  it('should call toast with error message if addStory or editStory fails', () => {
    // Arrange
    spyOn(component.storyForm, 'invalid' as any).and.returnValue(false);
    component.action = 'Add';
    component.storyForm.controls['storyNumber'].setValue('Story 1');
    component.storyForm.controls['description'].setValue('Story 1 Description');
    component.storyForm.controls['remarks'].setValue('');
    spyOn(component.service, 'addStory').and.returnValue(throwError({}));
    spyOn(component.service, 'toast');
  
    // Act
    component.onSubmit();
  
    });
});
