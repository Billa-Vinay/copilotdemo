import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { ApiServiceService } from './api-service.service';

@Injectable({
  providedIn: 'root'
})

export class AuthGuard implements CanActivate {
  constructor(private authService: ApiServiceService, private router: Router) { }
  canActivate() {
    if (!this.authService.isLoggedIn()) {
      this.router.navigate(['/login']); 
      return false;
    }
    return true;
  }

}